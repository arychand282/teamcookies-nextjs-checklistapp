import { Activity } from "../../interfaces";

interface SearchActivityProps {
  searchInput: string;
  setSearchInput: Function;
  activities: Activity[];
  setFilteredActivities: Function;
}

export default function SearchActivity({
  searchInput,
  setSearchInput,
  activities,
  setFilteredActivities,
}: SearchActivityProps) {

  const onSearchInputChange = (e: any) => {
    const keyword = e.target.value;
    setSearchInput(keyword);
    let regex = new RegExp(keyword, 'i');
    const filteredActivities = activities.filter(activity => {
      return regex.test(activity.name);
    });
    setFilteredActivities(filteredActivities);
  };

  return (
    <>
      <input
        type='text'
        placeholder='Search activity...'
        className='task-input-search'
        value={searchInput} onChange={onSearchInputChange}
      />
    </>
  );

}