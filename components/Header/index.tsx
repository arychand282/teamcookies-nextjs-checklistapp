import Link from "next/link";
import styles from './Header.module.css';

export default function Header() {
  return (
    <>
      <div>
        <header className={styles.container}>
          <ul className={styles.list}>
            <li className={styles.item}><Link href="/">Activity List</Link></li>
          </ul>
        </header>
      </div>
    </>
  );
}