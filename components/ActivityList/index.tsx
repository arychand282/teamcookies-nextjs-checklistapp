import { faCheckCircle, faEdit, faTrash } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { Activity } from "../../interfaces";

interface ActivityListProps {
  activities: Activity[];
  setActivities: Function;
  setEditActivity: Function;
}

export default function ActivityList({ activities, setActivities, setEditActivity }: ActivityListProps) {

  const handleComplete = (activity: Activity) => {
    setActivities(
      activities.map((item) => {
        if (item.id === activity.id) {
          return { ...item, isCompleted: !item.isCompleted };
        }
        return item;
      })
    );
  };

  const handleEdit = (({ id }: Activity) => {
    const findActivity = activities.find((activity) => activity.id === id);
    setEditActivity(findActivity);
  });

  const handleDelete = ({ id }: Activity) => {
    const filteredActivities = activities.filter((activity) => activity.id !== id);
    setActivities(filteredActivities);
    if (filteredActivities.length === 0) {
      localStorage.setItem('activities', JSON.stringify([]));
    }
  };
  return (
    <>
      <div>
        {activities.map((activity) => {
          return (
            <li className="list-item" key={activity.id}>
              <input type='text' value={activity.name} className={`list ${activity.isCompleted ? "complete" : ""}`} onChange={(e) => e.preventDefault()} />
              <div>
                <button className="button-complete task-button" onClick={() => handleComplete(activity)}>
                  <FontAwesomeIcon icon={faCheckCircle} />
                </button>
                <button className="button-edit task-button" onClick={() => handleEdit(activity)}>
                  <FontAwesomeIcon icon={faEdit} />
                </button>
                <button className="button-delete task-button" onClick={() => handleDelete(activity)}>
                  <FontAwesomeIcon icon={faTrash} />
                </button>
              </div>
            </li>
          );
        })}
      </div>

    </>
  );
}