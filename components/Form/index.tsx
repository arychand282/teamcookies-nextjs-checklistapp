import { useEffect } from 'react';
import { v4 as uuidv4 } from 'uuid';
import { Activity } from '../../interfaces';

interface FormProps {
  input: string;
  setInput: Function;
  activities: Activity[];
  setActivities: Function;
  editActivity: Activity | any;
  setEditActivity: Function;
}

export default function Form({ input, setInput, activities, setActivities, editActivity, setEditActivity }: FormProps) {

  const updateActivity = (id: string, editedName: string, isCompleted: boolean) => {
    const newActivity = activities.map((activity) => {
      return activity.id === id ? { id, name: editedName, isCompleted } : activity;
    });
    setActivities(newActivity);
    setEditActivity(null);
  };

  useEffect(() => {
    if (editActivity) {
      setInput(editActivity.name);
    } else {
      setInput('');
    }
  }, [setInput, editActivity]);

  const onInputChange = (e: any) => {
    setInput(e.target.value);
  };

  const onFormSubmit = (e: any) => {
    e.preventDefault();
    if (!editActivity) {
      setActivities([...activities, {
        id: uuidv4(),
        name: input,
        isCompleted: false,
      }]);
      setInput('');
    } else {
      updateActivity(editActivity.id, input, editActivity.isCompleted);
    }

  };

  return (
    <>
      <form onSubmit={onFormSubmit}>
        <input type='text' placeholder='Enter new activity...' className='task-input' value={input} required onChange={onInputChange} />
        <button className='button-add' type='submit'>{editActivity ? 'OK' : 'Add'}</button>
      </form>
    </>
  );
}