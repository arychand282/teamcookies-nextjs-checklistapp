import Image from 'next/image';
import { Detector } from 'react-detect-offline';
import img from '../../public/icons8-wi-fi-disconnected-100.png';

export default function CheckConnection(props: any) {

  return (
    <>
      <Detector
        render={({ online }) => (
          online ? props.children :
            <div style={{ paddingTop: '10px', textAlign: 'center' }}>
              <Image src={img} alt="not connected" />
              <h1 style={{ marginBottom: '5px' }}>No Connection</h1>
              <h4 style={{ margin: '0' }}>Please check your internet connection</h4>
            </div>
        )}
      />
    </>
  );
}