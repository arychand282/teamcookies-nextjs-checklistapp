import { useEffect, useState } from 'react';
import ActivityList from '../components/ActivityList';
import Form from '../components/Form';
import Layout from '../components/Layout';
import SearchActivity from '../components/SearchActivity';

export default function Home() {
  const [input, setInput] = useState('');
  const [activities, setActivities] = useState([]);
  const [editActivity, setEditActivity] = useState(null);
  const [searchInput, setSearchInput] = useState('');
  const [filteredActivities, setFilteredActivities] = useState([]);

  useEffect(() => {
    const activitiesFromLocalStorage = localStorage.getItem('activities');
    const parsedActivities = activitiesFromLocalStorage !== null ? JSON.parse(activitiesFromLocalStorage) : [];
    setActivities(parsedActivities);
  }, []);

  useEffect(() => {
    if (activities.length === 0) return;
    localStorage.setItem('activities', JSON.stringify(activities));
    setFilteredActivities(activities);
  }, [activities]);

  return (
    <>
      <Layout pageTitle='Home Page'>
        <div className='container'>
          <div className='app-wrapper'>
            <div className='header'>
              <h1>Activity List</h1>
            </div>
            <div>
              <Form
                input={input}
                setInput={setInput}
                activities={activities}
                setActivities={setActivities}
                editActivity={editActivity}
                setEditActivity={setEditActivity}
              />
            </div>
            <div>
              <SearchActivity
                searchInput={searchInput}
                setSearchInput={setSearchInput}
                activities={activities}
                setFilteredActivities={setFilteredActivities}
              />
            </div>
            <div>
              <ActivityList
                activities={filteredActivities}
                setActivities={setActivities}
                setEditActivity={setEditActivity}
              />
            </div>
          </div>
        </div>

      </Layout>
    </>
  );
}
