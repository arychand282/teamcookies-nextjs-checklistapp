This is a [Next.js](https://nextjs.org/) project bootstrapped with [`create-next-app`](https://github.com/vercel/next.js/tree/canary/packages/create-next-app).

## Getting Started

First, run the development server:

```bash
yarn dev
```

Open [http://localhost:3000](http://localhost:3000) with your browser to see the result.

## Run Using Docker
If you have Docker installed in your machine, you can run the app on Docker by running these command in the base directory:

```bash
$ docker build -t nextjs-docker .
```

```bash
$ docker run -p 3000:3000 nextjs-docker
```


