export interface Activity {
  id: string;
  name: string;
  isCompleted: boolean;
}